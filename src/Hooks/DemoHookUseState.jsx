import React, { useState } from "react";

export default function DemoHookUseState(props) {
  // Ứng với this.state và this. setState
  let [state, setState] = useState({
    like: 0,
  });
  return (
    <div className="container">
      <div className="card text-left">
        <img className="card-img-top" src="holder.js/100x180/" alt />
        <div className="card-body">
          <h4 className="card-title">Title</h4>
          <p className="card-text" style={{ color: "red" }}>
            {state.like} ♥
          </p>
        </div>
      </div>
      <button className="btn btn-primary">Like</button>
    </div>
  );
}

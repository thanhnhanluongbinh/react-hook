import logo from "./logo.svg";
import "./App.css";
import DemoHookUseState from "./Hooks/DemoHookUseState";

function App() {
  return <DemoHookUseState />;
}

export default App;
